package com.idea.self.amitpp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.facebook.ads.*;
import com.idea.self.amitpp.R;
import com.idea.self.amitpp.base.AppConstant;
import com.idea.self.amitpp.base.ConnectivityReceiver;
import com.idea.self.amitpp.base.MyApplication;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    //private AdView adView;
    private ConnectivityReceiver mConnectivityReceiver;
    private Activity mActivity;
    private NativeAdLayout nativeAdLayout;
    private LinearLayout adView;
    private ProgressDialog progressBar;
    private NativeAd nativeAd;
    private static final String TAG = DashboardActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        progressBar = ProgressDialog.show(mActivity, "", "Loading...");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dashboard);
        findViewById(R.id.imageView5).setOnClickListener(mOnClickListener);
        findViewById(R.id.imageView6).setOnClickListener(mOnClickListener);
        findViewById(R.id.imageView7).setOnClickListener(mOnClickListener);
        findViewById(R.id.imageView8).setOnClickListener(mOnClickListener);


        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
        mConnectivityReceiver = MyApplication.getInstance().getConnectivityReceiver();
        getSupportActionBar().setTitle(R.string.home);

        loadNativeAd();

    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (!mConnectivityReceiver.isConnected()) {
                Toast.makeText(mActivity, "Please check your internet connection.", Toast.LENGTH_LONG).show();
                return;
            }
            if (id == R.id.imageView5) {
                startActivity(new Intent(mActivity, SecondActivity.class));
            } else if (id == R.id.imageView7) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                //Try Google play
                intent.setData(Uri.parse("market://details?id=" + mActivity.getPackageName()));
                if (!rateApp(intent)) {
                    //Market (Google play) app seems not installed, let's try to open a webbrowser
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + mActivity.getPackageName()));
                    if (!rateApp(intent)) {
                        //Well if this also fails, we have run out of options, inform the user.
                        Toast.makeText(mActivity, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (id == R.id.imageView6) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(browserIntent);
            } else if (id == R.id.imageView8) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Line");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Message details......");//skype, whatsApp, linkdin, gmail etc.
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share by"));
            }
        }
    };

    private boolean rateApp(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
       /* if (mConnectivityReceiver.isConnected()) {
            Toast.makeText(mActivity, "connected", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mActivity, "Not", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void loadNativeAd() {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(this, AppConstant.NATIVE_ADD_ID);

        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
                if(progressBar != null && progressBar.isShowing()){
                    progressBar.dismiss();
                }
                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if(progressBar != null && progressBar.isShowing()){
                    progressBar.dismiss();
                }
                // Native ad is loaded and ready to be displayed
                Log.d(TAG, "Native ad is loaded and ready to be displayed!");
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                inflateAd(nativeAd);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        nativeAd.loadAd();
    }

    private void inflateAd(NativeAd nativeAd) {

        nativeAd.unregisterView();

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.native_ad_container);
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView = (LinearLayout) inflater.inflate(R.layout.native_ad_layout, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(mActivity, nativeAd, nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        //nativeAdBody.setVisibility(View.GONE);
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdSocialContext.setVisibility(View.GONE);
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }

}
