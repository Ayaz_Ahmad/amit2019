package com.idea.self.amitpp.adapter;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.ads.*;
import com.idea.self.amitpp.R;
import com.idea.self.amitpp.activity.DashboardActivity;
import com.idea.self.amitpp.base.AppConstant;
import com.idea.self.amitpp.base.Model;

import java.util.ArrayList;
import java.util.List;

//https://guides.codepath.com/android/Heterogenous-Layouts-inside-RecyclerView
//https://www.journaldev.com/12372/android-recyclerview-example
//https://stackoverflow.com/questions/26245139/how-to-create-recyclerview-with-multiple-view-type
//https://www.codexpedia.com/android/android-recyclerview-with-multiple-different-layouts/
public class SecondActivityAdapter extends RecyclerView.Adapter {

    private List<Model> dataSet;
    private Activity mActivity;
    private NativeAdLayout nativeAdLayout;
    private LinearLayout adView;
    private NativeAd nativeAd;
    private static final String TAG = SecondActivityAdapter.class.getName();
    private Handler handler;
    //declare interface
    private OnItemClicked onClick;

    //make interface like this
    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public static class NativeAddContainerViewHolder extends RecyclerView.ViewHolder {

        NativeAdLayout nativeAdLayout;
        //LinearLayout addContainer;

        public NativeAddContainerViewHolder(View itemView) {
            super(itemView);
            this.nativeAdLayout = (NativeAdLayout) itemView.findViewById(R.id.native_ad_container_view);
            //this.addContainer = (LinearLayout) itemView.findViewById(R.id.ad_container);
        }
    }

    public static class ImageTypeViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textView;

        public ImageTypeViewHolder(View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.logo_img);
            this.textView = (TextView) itemView.findViewById(R.id.text_view);
        }
    }

    public SecondActivityAdapter(List<Model> data, Activity context) {
        this.dataSet = data;
        handler = new Handler();
        this.mActivity = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case Model.LOGO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_1, parent, false);
                return new ImageTypeViewHolder(view);

            case Model.BANNER_AD:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_2, parent, false);
                //loadNativeAd(view);
                return new NativeAddContainerViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).type) {
            case 0:
                return Model.BANNER_AD;
            case 1:
                return Model.LOGO;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {
        Model object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.type) {
                case Model.BANNER_AD:
                    Log.d("Fraz", "onBindViewHolder: " + listPosition);
                    loadNativeAd(holder.itemView, ((NativeAddContainerViewHolder) holder).nativeAdLayout);
                    // ((NativeAddContainerViewHolder) holder).nativeAdLayout.setVisibility(View.GONE);
                    //((NativeAddContainerViewHolder) holder).linearLayout.setBackgroundColor(R.color.gray_dark);
                    break;
                case Model.LOGO:
                    ((ImageTypeViewHolder) holder).image.setImageResource(object.name);
                    //((ImageTypeViewHolder) holder).textView.setText(object.name);
                    ((ImageTypeViewHolder) holder).image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClick.onItemClick(listPosition);
                        }
                    });
                    break;
            }
        }


    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    private void loadNativeAd(final View view, final NativeAdLayout nativeAdLayout) {
        // Instantiate a NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(mActivity, AppConstant.NATIVE_ADD_ID);

        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Native ad is loaded and ready to be displayed
                Log.d(TAG, "Native ad is loaded and ready to be displayed!");
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container

                inflateAd(nativeAd, view, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        nativeAd.loadAd();
    }

    private void inflateAd(NativeAd nativeAd, View view, NativeAdLayout nativeAdLayout) {

        nativeAd.unregisterView();

        // Add the Ad view into the ad container.
        // nativeAdLayout = view.findViewById(R.id.native_ad_container_view);
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView = (LinearLayout) inflater.inflate(R.layout.native_ad_layout, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = view.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(mActivity, nativeAd, nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        //nativeAdBody.setVisibility(View.GONE);
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdSocialContext.setVisibility(View.GONE);
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);

        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }
}