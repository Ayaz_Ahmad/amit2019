package com.idea.self.amitpp.base;

public class Model {

    public static final int BANNER_AD =0;
    public static final int LOGO =1;

    public int type;
    public int name;
    public String url;

    public Model(int type, String url, int name)
    {
        this.type=type;
        this.name=name;
        this.url=url;
    }
}