package com.idea.self.amitpp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.idea.self.amitpp.R

class LauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** Hiding Title bar of this activity screen */
        getWindow()!!.requestFeature(Window.FEATURE_NO_TITLE)
        /** Making this activity, full screen */
        getWindow()!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_launcher)
        getSupportActionBar()!!.hide()

        launchDashboardActivity()
    }

    private fun launchDashboardActivity() {
        Handler().postDelayed({ startActivity(Intent(this@LauncherActivity, DashboardActivity::class.java))
            finish()
        }, 3000)

    }
}
