package com.idea.self.amitpp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.idea.self.amitpp.R;
import com.idea.self.amitpp.adapter.SecondActivityAdapter;
import com.idea.self.amitpp.base.AppConstant;
import com.idea.self.amitpp.base.Model;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends AppCompatActivity implements SecondActivityAdapter.OnItemClicked {
    private InterstitialAd interstitialAd;
    private static final String TAG = SecondActivity.class.getName();
    private List<Model> modelList;
    private ProgressDialog progressBar;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        progressBar = ProgressDialog.show(mActivity, "", "Loading...");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_second);
        getSupportActionBar().setTitle(R.string.second_page_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressBar != null) {
                    progressBar.dismiss();
                }
            }
        }, 5000);
        /////////
        //Instantiate an InterstitialAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        interstitialAd = new InterstitialAd(this, AppConstant.INTERSTITIAL_ADD_ID);
        // Set listeners for the Interstitial Ad
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                if (progressBar != null) {
                    progressBar.dismiss();
                }
                // Ad error callback
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (progressBar != null) {
                    progressBar.dismiss();
                }
                // Interstitial ad is loaded and ready to be displayed
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                // Check if ad is already expired or invalidated, and do not show ad if that is the case. You will not get paid to show an invalidated ad.
                if (interstitialAd.isAdInvalidated()) {
                    return;
                }
                // Show the ad
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();

        modelList = new ArrayList();

        modelList.add(new Model(Model.LOGO, getString(R.string.url_1), R.drawable.logo));//1
        modelList.add(new Model(Model.LOGO, getString(R.string.url_2), R.drawable.logo));//2
        modelList.add(new Model(Model.LOGO, getString(R.string.url_3), R.drawable.logo));//3
        modelList.add(new Model(Model.LOGO, getString(R.string.url_4), R.drawable.logo));//4
        modelList.add(new Model(Model.BANNER_AD, "", 0));
        modelList.add(new Model(Model.LOGO, getString(R.string.url_5), R.drawable.logo));//5
        modelList.add(new Model(Model.LOGO, getString(R.string.url_6), R.drawable.logo));//6
        modelList.add(new Model(Model.LOGO, getString(R.string.url_7), R.drawable.logo));//7
        modelList.add(new Model(Model.LOGO, getString(R.string.url_8), R.drawable.logo));//8
        modelList.add(new Model(Model.BANNER_AD, "", 0));
        modelList.add(new Model(Model.LOGO, getString(R.string.url_9), R.drawable.logo));

        SecondActivityAdapter adapter = new SecondActivityAdapter(modelList, this);
        @SuppressLint("WrongConstant")
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 6);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // 5 is the sum of items in one repeated section
                switch (position % 5) {
                    // first two items span 3 columns each
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        return 3;
                    case 4:
                        return 6;
                }
                throw new IllegalStateException("internal error");
            }
        });
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
        adapter.setOnClick(SecondActivity.this); // Bind the listener

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onDestroy() {
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onItemClick(int position) {
        // We will get url and title here using position
        Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
        intent.putExtra("url", modelList.get(position).url);
        startActivity(intent);

        //Toast.makeText(SecondActivity.this, "Position: "+position, Toast.LENGTH_SHORT).show();
    }
}